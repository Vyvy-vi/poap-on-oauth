# poap-on-oauth

This project aims to implement a version of the Proof Of Attendance Protocol, that doesn't rely on the Ethereum blockchain, and rather works on OAuth to applications like GitHub/Discord.


## QnA
### Q) Why is this on GitLab?
Ans- Usually I use, GitHub([Vyvy-vi](https://github.com/Vyvy-vi)), however trying out GitLab would be nice(esp. since they're essentially oss).

### Q) What does this project aim to do, and why?
Ans- [POAP](https://www.poap.xyz/) is a seemingly great protocol for ensuring verifiable meet-attendence and even badges that can be given out at conventions.
However, their is a minus to this, since it is meant for people in the crypto space and might not be a viable option for OSS communities.
This is much more like a project aiming at making unique, verifiable badges that can be provided to people on the basis of logins based on their
Github/Discord account. (Maybe even Gitlab? Idk how to set those on gitlab :/)

### Q) What tech stack might be used?
Ans- The following options seem viable-
- Django backend with React Frontend
- Vue.js/React.js with node.js backend
- Ruby on Rails

### Q) How is this going to be implemented?
Ans- This can take a couple of approaches:
1. Opening 1 Ethereum Wallet that mints and stores NFTs(preferably on xDAI chain) and keeping a ledger for every badge to set up corresponding badge owners and details.
2. Keeping a normal db with every badge having unique hash attributes and links to the badges stored on a cdn.
3. Making a db for users where badges are stored acc. to their github accounts and logging in to site needs them to use github auth
Not sure tho :/ 

### Q) What's the time-scale for this project?
Ans- This is gonna be a slow one, since I am busy with other projects.. (Also, I need to get good at frontend first :P)
Work would probably start June/July and _might_ be in production by December.

